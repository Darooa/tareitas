import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserServicesService } from 'src/app/services/user-services.service';


@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css']
})
export class ProyectoComponent implements OnInit {
  idPro: number;
  proyecto: any ;
  constructor(private rutaAct:ActivatedRoute, 
    private services:UserServicesService) { }

  ngOnInit(): void {
    this.idPro = this.rutaAct.snapshot.params.id;
    this.services.getProyecto(this.idPro).
    subscribe((proyecto: any) =>{
      this.proyecto = proyecto.data;
      console.log(proyecto);
    });
  }

}
